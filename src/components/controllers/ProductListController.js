import React, { useEffect } from "react";
import api from "../../api";
import { connect } from "react-redux";

function ProductListController({
  children,
  initialize,
  products = [],
  removeProduct,
  editProduct,
}) {
  useEffect(() => {
    if (products.length === 0) loadSeedData();
    return () => {};
  }, []);

  async function loadSeedData() {
    const products = await api.fetchProducts();
    initialize(products);
  }

  return React.cloneElement(children, {
    products: products,
    removeProduct: removeProduct,
    editProduct: editProduct,
  });
}

function mapStateToProps(state) {
  const {
    products: { products, productIds },
    prices: { prices },
  } = state;
  console.log(productIds);
  return {
    products: productIds.map((prodId) => {
      return { ...products[prodId], price: prices[prodId][0] };
    }),
  };
}

function mapDispatchToProps(dispatch) {
  return {
    initialize: (products) => dispatch({ type: "init", data: products }),
    removeProduct: (productId) =>
      dispatch({ type: "delete_product", productId: productId }),
    editProduct: (productId) =>
      dispatch({ type: "select_edit_product", productId: productId }),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductListController);
