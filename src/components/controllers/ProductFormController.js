import React from "react";
import { connect } from "react-redux";

function ProductFormController({
  children,
  addProduct,
  saveProduct,
  productInFocus,
}) {
  return React.cloneElement(children, {
    onSave: productInFocus ? saveProduct : addProduct,
    existingProduct: productInFocus,
  });
}

function mapStateToProps(state) {
  const {
    products: { productIds, productInFocus },
    prices: { prices },
  } = state;
  console.log(productIds);
  if (productInFocus) {
    return {
      productInFocus: {
        productName: productInFocus.name,
        productPrice: prices[String(productInFocus.id)][0].price,
      },
    };
  }
  return {
    productInFocus: undefined,
  };
}

function saveProductThunk(name, price) {
  return (dispatch, getState) => {
    const state = getState();
    const {
      products: { productInFocus = {} },
    } = state;

    dispatch({
      type: "save_product",
      productId: productInFocus.id,
      name: name,
      price: price,
    });
  };
}

function mapDispatchToProps(dispatch) {
  return {
    addProduct: ({ productName, productPrice }) =>
      dispatch({
        type: "add_product",
        productId: Date.now(),
        name: productName,
        price: productPrice,
      }),
    saveProduct: ({ productName, productPrice }) => {
      dispatch(saveProductThunk(productName, productPrice));
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductFormController);
