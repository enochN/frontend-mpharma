import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import ProductFormController from "./controllers/ProductFormController";

export function ProductFormView({
  onSave = (data) => console.log(data),
  existingProduct = {},
}) {
  const { register, handleSubmit, reset } = useForm({
    defaultValues: {},
  });

  console.log(existingProduct);

  useEffect(() => {
    if (existingProduct && existingProduct.productName) reset(existingProduct);
    return () => {};
  }, [existingProduct]);

  function handleSave(data, e) {
    onSave(data);
    e.target.reset();
  }

  return (
    <form className="product-form" onSubmit={handleSubmit(handleSave)}>
      <div className="input-section">
        <label htmlFor="productName">Product Name</label>
        <input
          type="text"
          name="productName"
          id="productName"
          className="product-input productName"
          ref={register({ required: true })}
        />
      </div>

      <div className="input-section">
        <label htmlFor="productPrice">Product Price</label>
        <input
          type="number"
          min="0"
          step=".01"
          name="productPrice"
          id="productPrice"
          className="product-input productPrice"
          ref={register({ required: true })}
        />
      </div>
      <button type="submit">Save Product</button>
    </form>
  );
}

function ProductForm() {
  return (
    <ProductFormController>
      <ProductFormView />
    </ProductFormController>
  );
}

export default ProductForm;
