import React from "react";
import ProductListController from "./controllers/ProductListController";

function ProductList() {
  return (
    <ProductListController>
      <ProductListView />
    </ProductListController>
  );
}

export function ProductListView({
  products = [],
  removeProduct = () => {},
  editProduct = () => {},
}) {
  return (
    <div>
      {products.map(({ id, name, price }) => (
        <div className="product" key={id}>
          <h4 className="product-name">{name}</h4>
          <p className="product-price">
            Selling at <b> GHS {price.price}</b>
          </p>
          <p className="product-date">
            Last updated {new Date(price.date).toLocaleString()}
          </p>
          <div className="actions">
            <button onClick={() => editProduct(id)}>Edit</button>
            <button onClick={() => removeProduct(id)}>Delete</button>
          </div>
        </div>
      ))}
      {products.length === 0 && (
        <h3 style={{ textAlign: "center" }}>No Products to display</h3>
      )}
    </div>
  );
}

export default ProductList;
