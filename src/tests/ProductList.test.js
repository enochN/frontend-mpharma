import React from "react";
import { render } from "@testing-library/react";
import { ProductListView } from "../components/ProductList";

test("renders products", async () => {
  const { getByText } = render(
    <ProductListView
      products={[
        {
          id: 2,
          name: "JOHN",
          price: {
            price: 100,
            date: "Mon Aug 24 2020 16:45:42 GMT+0000 (Greenwich Mean Time)",
          },
        },
        {
          id: 3,
          name: "AMA",
          price: {
            price: 50,
            date: "Mon Aug 17 2019 16:45:42 GMT+0000 (Greenwich Mean Time)",
          },
        },
      ]}
    />
  );

  const productTitle = getByText(/john/i);
  const productPrice = getByText(/100/i);
  const productDate = getByText(/2020/i);
  expect(productPrice).toBeInTheDocument();
  expect(productTitle).toBeInTheDocument();
  expect(productDate).toBeInTheDocument();
});
