export function sortCompareDates(a, b) {
  let timestampA = new Date(a).getTime();
  let timestampB = new Date(b).getTime();

  if (timestampA < timestampB) {
    return 1;
  }
  if (timestampA > timestampB) {
    return -1;
  }
  return 0;
}
