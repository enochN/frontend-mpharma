import React from "react";
import ProductList from "../components/ProductList";
import ProductForm from "../components/ProductForm";

function Home() {
  return (
    <div className="container">
      <div className="products-wrapper">
        <ProductList />
      </div>
      <div className="add-product-wrapper">
        <ProductForm />
      </div>
    </div>
  );
}

export default Home;
