import { sortCompareDates } from "../utils";

const initialState = {
  prices: {},
};

function sortProductPrices(a, b) {
  return sortCompareDates(a.date, b.date);
}

export default function prices(state = initialState, action) {
  switch (action.type) {
    case "init":
      const { data } = action;
      const { prices } = state;
      data.forEach((prod) => {
        prod.prices.sort(sortProductPrices);
        prices[String(prod.id)] = prod.prices;
      });

      const finalState = {
        ...state,
        prices,
      };
      return finalState;
    case "add_product":
      const { productId: newProductId, price } = action;
      console.log(state);

      state.prices[String(newProductId)] = [
        {
          price,
          date: new Date().toString(),
          id: Date.now(),
          productId: newProductId,
        },
      ];

      return {
        ...state,
        prices: state.prices,
      };
    case "save_product":
      const { price: newPrice, productId: productIdInFocus } = action;

      state.prices[String(productIdInFocus)].unshift({
        price: newPrice,
        date: new Date().toString(),
        id: Date.now(),
        productId: productIdInFocus,
      });

      return {
        ...state,
        prices: state.prices,
      };
    default:
      return state;
  }
}
