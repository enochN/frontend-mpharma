const initialState = {
  products: {},
  productIds: [],
};

export default function products(state = initialState, action) {
  switch (action.type) {
    case "init":
      const { data } = action;
      const { products, productIds } = state;
      data.forEach((prod) => {
        productIds.push(String(prod.id));
        products[String(prod.id)] = { id: Number(prod.id), name: prod.name };
      });

      const finalState = {
        ...state,
        products,
        productIds,
      };
      return finalState;
    case "add_product":
      const { name, productId: newProductId } = action;

      state.productIds.unshift(String(newProductId));
      state.products[String(newProductId)] = { id: newProductId, name: name };

      return {
        ...state,
        products: state.products,
        productIds: state.productIds,
      };
    case "delete_product":
      const { productId: productIdToDelete } = action;

      let updatedProductIds = state.productIds.filter(
        (prodId) => prodId != productIdToDelete
      );
      delete state.products[String(productIdToDelete)];

      return {
        ...state,
        products: state.products,
        productIds: updatedProductIds,
      };
    case "select_edit_product":
      const { productId: productIdToEdit } = action;

      return {
        ...state,
        productInFocus: state.products[String(productIdToEdit)],
      };
    case "save_product":
      const { name: newName, productId: productIdInFocus } = action;

      state.products[String(productIdInFocus)].name = newName;

      return {
        ...state,
        products: state.products,
        productInFocus: undefined,
      };
    default:
      return state;
  }
}
