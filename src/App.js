import React from "react";
import Home from "./pages/Home";
import { Provider } from "react-redux";
import store from "./store";
import { PersistGate } from "redux-persist/integration/react";

function App() {
  return (
    <Provider store={store.store}>
      <PersistGate loading={null} persistor={store.persistor}>
        <Home />
      </PersistGate>
    </Provider>
  );
}

export default App;
