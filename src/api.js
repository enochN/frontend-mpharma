import Axios from "axios";

const REMOTE_API = "https://www.mocky.io/v2/5c3e15e63500006e003e9795";

async function fetchProducts() {
  const response = await Axios.get(REMOTE_API);
  return response.data.products;
}

export default { fetchProducts };
